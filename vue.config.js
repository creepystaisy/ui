module.exports = {
  publicPath: process.env.NODE_ENV !== 'production' ? 'ui' : '',
  productionSourceMap: true,
  transpileDependencies: [
    'vuetify'
  ]
}
