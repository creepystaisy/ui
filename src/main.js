import Vue from 'vue'
import router from './router'
import App from './App.vue'
import './registerServiceWorker'
import store from './store'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

if (process.env.VUE_APP_CI_COMMIT) {
  window.VUE_APP_CI_COMMIT = process.env.VUE_APP_CI_COMMIT
}

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
