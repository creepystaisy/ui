import Vue from 'vue'
import VueRouter from 'vue-router'
import DefaultLayouts from '../layouts/default.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: DefaultLayouts,
    meta: {
      title: 'default title router/index.js'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})

export default router
